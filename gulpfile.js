const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const stylus = require('gulp-stylus');
const jade = require('gulp-pug');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');

var paths = {
    js: [
        './private/js/*.js',
    ],
    jade: [
        './private/jade/*.jade'
    ],
    stylus: [
        './private/stylus/*.styl'
    ],
    img: [
        './private/img/*'
    ]
};

function handleError(error) {
    console.log(error.toString());
}

gulp.task('default', ['js', 'stylus', 'jade', 'images', 'watch']);

gulp.task('js',() => {
	return gulp.src(paths.js)
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./public/js/'))
        .on('error', handleError);
});

gulp.task('stylus', () => {
  return gulp.src(paths.stylus)
        .pipe(stylus({
            compress: true,
        }))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./public/css/'))
        .on('error', handleError);
});

gulp.task('jade', () => {
    return  gulp.src(paths.jade)
        .pipe(jade())
        .pipe(gulp.dest('./views/'))
        .on('error', handleError);
});

gulp.task('images', () => {
    return gulp.src(paths.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            //use: [pngquant(), imageminJpegtran({progressive: true})]
        }))
        .pipe(gulp.dest('./public/img/'))
        .on('error', handleError);;
});

gulp.task('watch', () => {
    gulp.watch(paths.js, ['js']);
    gulp.watch(paths.stylus, ['stylus']);
    gulp.watch(paths.jade, ['jade']);
    gulp.watch(paths.img, ['images']);
});