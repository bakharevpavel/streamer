'use strict';
const views = require('./controllers/views');
const compress = require('koa-compress');
const logger = require('koa-logger');
const serve = require('koa-static');
const route = require('koa-route');
const koa = require('koa');
const path = require('path');
const app = new koa();
const https = require('https');
const fs = require('fs');

var privateKey = fs.readFileSync('fakekeys/privatekey.pem').toString(),
    certificate = fs.readFileSync('fakekeys/certificate.pem').toString();

// Logger
app.use(logger());

app.use(route.get('/', views.home));

// Serve static files
app.use(serve(path.join(__dirname, 'public')));

// Compress
app.use(compress());

if (!module.parent) {
  	https.createServer({key: privateKey, cert: certificate}, app.callback()).listen(4000);
  	console.log('listening on port 4000');
}
